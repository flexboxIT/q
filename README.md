# Q

Q is a simple wrapper around Git, aimed to save your time. For example, instead of `git push origin master` you can do `qp` in master.

### Installation

To install Q, add `bin` folder to your path.

* Linux users add the Q folder into `PATH` in your `~/.bashrc` (if you are using `bash`) or `~/.zshrc` (if you are using `zsh`).
* Mac users add the Q folder to `/etc/paths`.
* Windows users run `set PATH=%PATH%;[Q bin folder]`

### Commands

* `qhelp` - displays help for `q`
* `qc` - performs a `git commit` command.
* `qch` - performs a `git checkout` command.
* `qchp` - performs a `git checkout` command, followed by `git pull`.
* `qp` - takes the name of current branch and performs a `git pull origin <current branch>` command.
* `qpl` - performs a `git pull` command
* `qr <number>` - performs a rebase. If you supply a number, the script would run an interactive rebase, i.e. `git rebase -i HEAD~<number>`. Default `number` is set to `2`. But if you supply a name of a branch, the script would simply run `git stash && git rebase <branch> && git stash apply`.
* `qs` - performs a `git status` command.
